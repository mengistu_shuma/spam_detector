# CyberSecurity final project
# Spam Email Detector using Machine Learning
# John Pace
# Kevin Macfarlane
# Mengistu Shuma
# Group 6
# Fall 2019



# Installation 
please install all the requirement libraries and import

from bs4 import BeautifulSoup
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import GridSearchCV
from sklearn import svm
import pandas as pd
import pickle
import imaplib
import email
from nltk.corpus import wordnet
from nltk.corpus import stopwords
import string
import os
import csv


# how to run the program 
make use you have all the file in the cyberSecurityP file

	email.csv -  generated in the correct format to train the SVM algorithm
	final_model.sav - SVM mode to predict the email data. the mode is train on 4138 emails data test on 1035 
			  based on 80% train and 20% test it has an accuracy of .92
	email - email folder contain raw email data. note the data is not in the right format to train the algorithm. to train the algorithm we 
		have to extract into correct format.
	svm.py - is a source code


the program is build on menu base. After we run the program we fin the following menu 
	1. Extract the raw emails and generate csv file - to generate csv file if it is not already generated (it is already generated no need to do)
	2. Train the SVM				- to build the model if it is not already built (it is already built no need to make a model
	3. Test the accuracy of the algorithm 		- to test the accuracy and email dataset on the provided dataset
	4. Predict for single email 			- to predict email data from actual email
	please enter your choice -->
	 
	

