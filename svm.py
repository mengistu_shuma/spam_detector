# CyberSecurity final project
# Spam Email Detector using Machine Learning
# John Pace
# Kevin Macfarlane
# Mengistu Shuma
# Group 6
# Fall 2019


#!/usr/bin/python
# Please install all library needed for this project
from bs4 import BeautifulSoup
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import GridSearchCV
from sklearn import svm
import pandas as pd
import pickle
import imaplib
import email
from nltk.corpus import wordnet
from nltk.corpus import stopwords
import string
import os
import csv

# global to SVM
MIN_SVM_1 = 1e-3
MIN_SVM_2 = 1e-5
FinalModel = "final_model.sav"

# clean punctuation and symbols for the text
def text_cleanup(text):
    # get rid all punctuation
    clean_text = [c for c in text if c not in string.punctuation]
    # join all words
    clean_text = ''.join(clean_text)
    text_word = [word for word in clean_text.split() if word.lower() not in stopwords.words('english')]
    text_word = ' '.join(text_word)
    cleaned_text = [word.lower() for word in text_word.split()]
    return cleaned_text  # return cleaned next

# extract data from directory and save into csv file
def extract():
    # directory path
    path = "emails/"
    csv_file = r"emails.csv"
    all_files = os.listdir(path)
    all_files.pop(0)  # get rid of the first file

    # check if the .txt file is ham or spam
    for file in all_files:

        # check if the file is ham/ spam and label the email
        txt_file = path + file
        if 'ham' in file:
            tmp = 'ham'
        else:
            tmp = 'spam'

        # open and read all the file and clean the text
        with open(txt_file, encoding="utf8", errors='ignore') as in_txt:
            out_csv = csv.writer(open(csv_file, 'a'))
            file_string = in_txt.read()
            line = " ".join(text_cleanup(file_string))
            writer = csv.writer(open(csv_file, 'a'), lineterminator='\n')
            writer.writerow([tmp, line])  # write a csv file into emais.csv

# once we generate the file we don't need this function
# extract()

# read from directory and pass it into emails.csv file
def extracEamil():
    csv_file = r"emails.csv"
    # get the pass
    path = "emails/"
    all_files = os.listdir(path)
    all_files.pop(0)
    tmp = ''
    for file in all_files:
        txt_file = path + file
        # check if the file is ham/ spam and label the email
        if 'ham' in file:
            tmp = 'ham'
        else:
            tmp = 'spam'
        print("===========", tmp)
        # open and read all the file and clean the text
        with open(txt_file, encoding="utf8", errors='ignore') as in_txt:
            temp = ''
            out_csv = csv.writer(open(csv_file, 'a'))
            file_string = in_txt.read()

            # loop though the line of the text and get rid of -
            for x in range(len(file_string)):
                if x != -1:
                    file_line = file_string[x].split('\n')
                    for y in range(len(file_line)):
                        if file_line[y] != '-':
                            temp = temp + file_line[y]

            # write to the file
            writer = csv.writer(open(csv_file, 'a'), lineterminator='\n')
            writer.writerow([tmp, temp]) # writ in one row

    return 0



# read from file to panda dataframe
def spamAndHarmFile():
    # read a spam and harm data from csv file to pandas
    dataframe = pd.read_csv("emails.csv")
    return dataframe


#
def trainSVM(dataframe):
    # split the data frame to x and y to train the algorithm
    x = dataframe["Data"]
    y = dataframe["Label"]

    # train the first 4000 data svm
    x_train, y_train = x[0:4138], y[0:4138]
    x_test, y_test = x[4138:], y[4138:]



    # Extract Features
    cv = CountVectorizer()
    features = cv.fit_transform(x_train)

    # provide all SVM model to a best model model
    tuned_parameters = {'kernel': ['rbf', 'linear'], 'gamma': [MIN_SVM_1, MIN_SVM_2], 'C': [1, 10, 100, 1000]}
    model = GridSearchCV(svm.SVC(), tuned_parameters)

    print("Training the SVM please wait ....")
    model.fit(features, y_train)

    print("Best model found is ", model.best_params_)
    # Test Accuracy

    print(model.score(cv.transform(x_test), y_test))

    pickle.dump(model, open(FinalModel, 'wb'))
    print("You have successfully save your model")

    return model


def testAccuracy(dataframe):
    # get the model from pickle
    model = pickle.load(open(FinalModel, 'rb'))
    # label the data frame
    x = dataframe["Data"]
    y = dataframe["Label"]

    # split the x and y train 80%
    x_train, y_train = x[0:4138], y[0:4138]
    cv = CountVectorizer()
    # fit the counter vector
    cv.fit_transform(x_train)
    # test the trained data on 20%
    x_test, y_test = x[4138:], y[4138:]

    # get the fist 10 test datas
    x_massage_test = x_test[0:10]
    y_message_test = y_test[0:10]

    # print the accurecy of the model
    print("\nAccurecy of the model is =-=->> ", model.score(cv.transform(x_test), y_test))

    # display the first 10 test data with their type to compare with the model prediction
    for i in range(len(x_massage_test)):
        print("Original Email =>", x_massage_test.iloc[i])
        print("Original email type => ", y_message_test.iloc[i])

        email_ = pd.Series(x_massage_test.iloc[i])
        print("Email prediction -> ===>>>> ", model.predict(cv.transform(email_)))

    return 0


# https://www.google.com/settings/security/lesssecureapps
# turn on Allow less secure apps: ON on your gmail if you are not connected to your gmail

def connectToGamil(email_user, pass_word):

    '''
    # please make sure to enter email address for user and
    a email address password for the password variable

    :return:

    '''
    user = email_user
    password = pass_word
    # connect to email provided
    mail = imaplib.IMAP4_SSL("imap.gmail.com")
    # login to the email using imaplib library
    mail.login(user, password)
    # get the email box
    mail.select("INBOX")
    result, data = mail.uid('search', None, "ALL")
    inbox_item_list = data[0].split()

    # temp email container
    first_ten_emails = []
    count_email = 0
    # featch all emails starting from most recent one
    for i in reversed(inbox_item_list):
        resultE, email_data = mail.uid('fetch', i, '(RFC822)')
        rawEmail = email_data[0][1].decode("utf-8", errors='ignore')


        # at this point we can get the sender, subject of the email and all body
        # the body is not yer extracted
        actual_message = email.message_from_string(rawEmail)
        email_form = actual_message['From']
        email_subject = actual_message['Subject']

        temp = ''
        count = 1
        # extract the body of the email
        for part in actual_message.walk():
            # check if the email has multi part if so continue and get all the parts
            if part.get_content_maintype() == "multipart":
                continue
             # get the filename. note the file name is multipart or none
            filename = part.get_filename()
            # if it is none get all the extract the data
            if not filename:
                ext = 'html'
                if not ext:
                    ext = '.bin'
                    filename = 'msg-part-%08d%s' % (count, ext)
            count += 1
            # get the content of the email
            contentType = part.get_content_type()

            # if the content contain pain it is empty so pass
            if "plain" in contentType:
                pass
            # if content contain html, extract the email
            elif "html" in contentType:
                # get the payload of the html part
                html_ = part.get_payload()
                # parse the html file using BeautifulSoup library
                getData = BeautifulSoup(html_, "html.parser")
                # get the text from the email
                text_email = getData.get_text()
                # call a text_cleanup function to symbols and junk
                cleaned = text_cleanup(text_email)
                # clean the subject of the email
                clean_subject = text_cleanup(email_subject)
                list_cleand = []
                # check if all cleaned word is english word
                for word in cleaned:
                    if word in wordnet.words():
                        list_cleand.append(word)

                # join all word together
                line = " ".join(list_cleand)
                temp = "subject" + " " + " ".join(clean_subject) + " " + line
                # print(temp)
        # save all cleaned text to array
        first_ten_emails.append(temp)
        #print(temp)
        #print(email_form)

        # get 7 most recent emails
        count_email += 1
        if count_email == 10:
            break
    return first_ten_emails # return array


def predictemail(dataframe, email_user, pass_word):
    # get the model from the pickle library
    model = pickle.load(open(FinalModel, 'rb'))
    # label the data frame
    x = dataframe["Data"]
    y = dataframe["Label"]
    # split the data train to x and y
    x_train, y_train = x[0:4138], y[0:4138]
    cv = CountVectorizer()
    # fit the counter vectorizer
    cv.fit_transform(x_train)

    # get the first 7 emails from email provided and loop though
    first_seven_emails = connectToGamil(email_user, pass_word)
    for data in range(len(first_seven_emails)):
        print(first_seven_emails[data])
        email_pd = pd.Series(first_seven_emails[data])
        # predict for each 7 email if it is spam or ham
        print("Predict  email ", data+1, " ==>> ", " ".join(model.predict(cv.transform(email_pd))))
    return 0

# main function
def main():


    # user interface.
    print("\t\tMenu\n1. Extract the raw emails and generate csv file \n2. Train the SVM\n"
          "3. Test the accuracy of the algorithm\n4. Classify last 10 emails ")
    choice = int(input("please enter your choice -->"))  # obtain the user input

    # get email data from csv file and save into panda dataframe
    data = spamAndHarmFile()
    # randomise_data = data.sample(frac=1)
    # describe the dataframe
    #print(data.describe(), '\n')
    # display the first 10 email data from dataframe
    #print(data.head(10))



    # once we generate the file we don't need have to generate the file
    if choice == 1:
        extracEamil()
    # Train the SVM and make a model
    if choice == 2:
        trainSVM(data)
    # check the accuracy and test the data
    elif choice == 3:
        testAccuracy(data)
    # predict data email using the model
    elif choice == 4:
        # obtain the user name and pass word
        email_user = input("Please enter an email address -- >")
        pass_word = input("Enter the email password --> ")
        predictemail(data, email_user, pass_word)
    else:
        print("Goodbye!")
# run main
main()
