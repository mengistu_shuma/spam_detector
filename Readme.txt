
# Installation 
The following libraries must be installed before running the program:

bs4
sklearn
pandas
pickle
imaplib
email
nltk.corpus


# To run:

Files used:
	emails.csv -  generated in the correct format to train the SVM algorithm
	final_model.sav - Trained SVM model to classify email data. This model has been trained on 80% of the enron email dataset and tested on the remaining 20%. It has an accuracy of 92%.
	emails - email folder containing raw email data. Note that the data is not in the right format to train the algorithm. To train the algorithm this data must be extracted into a labeled csv.(this is already done in email.csv).
	svm.py - Source code for main program.

Make sure that 'final_model.sav', 'emails.csv', and the 'emails' folder are in the same directory as the main program('svm.py').

The program is menu based. After starting the user should be presented with the following options:
	1. Extract the raw emails and generate csv file - to generate csv file if it is not already generated (it is already generated, no need to do)
	2. Train the SVM				- to build the model if it is not already built (it is already built no need to retrain the model)
	3. Test the accuracy of the algorithm 		- to test the accuracy of the model on the provided dataset
	4. Predict last 10 emails 			- to classify the last 10 emails as either spam or hame

After selecting option '4' the user will be prompted for an email address and password. We used an email account created specifically for this project, so it is recommended the reviewer does the same. They will also need to give the email account used the correct permissions for imaplib to correctly connect and retrieve emails. This process may vary depending on the email service and browser used. If this is done correctly the program will retrieve the last 10 emails from that account and classify them as either spam or ham. Note: There is currently no input validation, so the program will close if input incorrectly.
	 
	

